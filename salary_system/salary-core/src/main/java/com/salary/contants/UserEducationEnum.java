package com.salary.contants;

public enum UserEducationEnum {
    ONE("1","专科"),
    TWO("2","本科"),
    THREE("3","研究生"),
    FOUR("4","博士"),
    ;
    private String code;
    private String name;

    UserEducationEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
