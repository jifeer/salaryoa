package com.salary.modules.api.resultjson;

import com.salary.modules.salary.entity.SalaryDetail;
import com.salary.modules.salary.entity.SalaryRecord;

import java.io.Serializable;
import java.util.List;


public class SalaryDetailResult implements Serializable {
    //薪资记录表
    private SalaryRecord salaryRecord;
    //薪资明细表
    private List<SalaryDetail> salaryDetails;

    public SalaryRecord getSalaryRecord() {
        return salaryRecord;
    }

    public void setSalaryRecord(SalaryRecord salaryRecord) {
        this.salaryRecord = salaryRecord;
    }

    public List<SalaryDetail> getSalaryDetails() {
        return salaryDetails;
    }

    public void setSalaryDetails(List<SalaryDetail> salaryDetails) {
        this.salaryDetails = salaryDetails;
    }
}
