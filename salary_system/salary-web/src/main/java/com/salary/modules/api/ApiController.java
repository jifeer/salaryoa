package com.salary.modules.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.salary.core.R;
import com.salary.modules.api.resultjson.SalaryDetailResult;
import com.salary.modules.salary.entity.SalaryDetail;
import com.salary.modules.salary.entity.SalaryRecord;
import com.salary.modules.salary.entity.WorkRecord;
import com.salary.modules.salary.service.INoticeService;
import com.salary.modules.salary.service.ISalaryDetailService;
import com.salary.modules.salary.service.ISalaryRecordService;
import com.salary.modules.salary.service.IWorkRecordService;
import com.salary.modules.sys.user.entity.SysUser;
import com.salary.modules.sys.user.service.ISysUserService;
import com.salary.util.CryptoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * api接口
 *
 * @author sun
 */
@RestController
@RequestMapping(value = "/api")
public class ApiController {
    @Value("${up_time}")
    String standardUpTime;
    @Value("${down_time}")
    String standardDownTime;
    @Value("${over_time}")
    String standardOverTime;

    @Autowired
    private ISalaryDetailService salaryDetailService;

    @Autowired
    private ISalaryRecordService salaryRecordService;

    @Autowired
    private INoticeService noticeService;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private IWorkRecordService workRecordService;


    @PostMapping(value = "/wxDoLogin")
    @ResponseBody
    public Object wxDoLogin(String account, String password) {
        QueryWrapper queryWrapper = new QueryWrapper();
        SysUser sysUser = new SysUser();
        sysUser.setUserNo(account);
        queryWrapper.setEntity(sysUser);
        SysUser user = sysUserService.getOne(queryWrapper);
        if (null != user) {
            boolean verify = CryptoUtils.verify(password, user.getUserPwd());
            if (verify) {
                return R.success(user);
            }
        }
        return R.error("用户不存在！！！");
    }

    /**
     * 获取用户工资明细
     *
     * @param employeeId
     * @param searchDate
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/getXZDetail")
    @ResponseBody
    public Object getXZDetail(Integer employeeId, String searchDate) {
        QueryWrapper queryWrapper = new QueryWrapper();
        //根据用户id以及月份查询出记录id
        SalaryRecord salaryRecord = new SalaryRecord();
        salaryRecord.setEmployeeId(employeeId);
        salaryRecord.setSalaryDate(searchDate);
        queryWrapper.setEntity(salaryRecord);
        SalaryRecord salaryRecordReuslt = salaryRecordService.getOne(queryWrapper);
        if (null != salaryRecordReuslt) {
            //根据记录id查询明细表中的list集
            SalaryDetail detail = new SalaryDetail();
            detail.setRecordId(salaryRecordReuslt.getId());
            queryWrapper.setEntity(detail);
            queryWrapper.orderByAsc("id");
            List<SalaryDetail> salaryDetailList = salaryDetailService.list(queryWrapper);
            if (!CollectionUtils.isEmpty(salaryDetailList)) {
                SalaryDetailResult salaryDetailReesult = new SalaryDetailResult();
                salaryDetailReesult.setSalaryRecord(salaryRecordReuslt);
                salaryDetailReesult.setSalaryDetails(salaryDetailList);
                return R.success(salaryDetailReesult);
            }
        }
        return R.error("暂无明细");
    }

    /**
     * 获取公告列表
     *
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/getNotices")
    @ResponseBody
    public Object getNotices() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("id");
        List list = noticeService.list(queryWrapper);
        if (!CollectionUtils.isEmpty(list)) {
            return R.success(list);
        }
        return R.error("暂无明细");
    }

    /**
     * 获取考勤列表
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/getWorkRecords")
    @ResponseBody
    public Object getWorkRecords(Integer employeeId) {
        WorkRecord workRecord = new WorkRecord();
        workRecord.setEmployeeId(employeeId);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.setEntity(workRecord);
        queryWrapper.orderByDesc("id");
        List list = workRecordService.list(queryWrapper);
        if (!CollectionUtils.isEmpty(list)) {
            return R.success(list);
        }
        return R.error("暂无明细");
    }


        @PostMapping(value = "/goToWork")
    @ResponseBody
    public Object goToWork(Integer employeeId,String workDate,String upTime) {
        WorkRecord r = new WorkRecord();
        r.setEmployeeId(employeeId);
        r.setWorkDate(workDate);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.setEntity(r);
        WorkRecord one = workRecordService.getOne(queryWrapper);
        if (null != one) {
            return R.error(workDate + "上班打卡重复");
        }
        int upTimes = Integer.valueOf(upTime.substring(0, 2));
        Integer uTime = Integer.valueOf(standardUpTime);
        Integer minute = Integer.valueOf(upTime.substring(3, 5));
        if (upTimes > uTime || (upTimes == uTime && minute > 0)) {
            //迟到
            r.setWorkStatus(2);
        }
        r.setWorkMonth(workDate.substring(0, 7));
        r.setUpTime(upTime);
        boolean save = workRecordService.save(r);
        if (save) {
            return R.success("打卡成功");
        }
        return R.error("打卡失败");
    }

    @PostMapping(value = "/goOffWork")
    @ResponseBody
    public Object goOffWork(Integer employeeId,String workDate,String downTime) {
        WorkRecord r = new WorkRecord();
        r.setEmployeeId(employeeId);
        r.setWorkDate(workDate);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.setEntity(r);
        WorkRecord one = workRecordService.getOne(queryWrapper);
        if (null == one) {
            return R.error("请先打" + workDate + "上班卡,再打下班卡");
        }
        int downTimes = Integer.valueOf(downTime.substring(0, 2));
        if (downTimes < Integer.valueOf(standardDownTime)) {
            //早退
            r.setWorkStatus(3);
        } else if (downTimes >= Integer.valueOf(standardOverTime)) {
            //加班
            r.setWorkStatus(4);
        }
        r.setId(one.getId());
        r.setDownTime(downTime);
        boolean update = workRecordService.updateById(r);
        if (update) {
            return R.success("打卡成功");
        }
        return R.error("打卡失败");
    }
}
