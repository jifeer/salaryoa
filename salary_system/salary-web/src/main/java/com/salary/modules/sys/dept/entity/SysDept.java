package com.salary.modules.sys.dept.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.salary.modules.sys.menu.entity.SysMenu;

import java.io.Serializable;
import java.util.List;


public class SysDept implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("p_id")
    private Integer pId;

    private String name;

    private Integer orderNum;
    @TableField(exist = false)
    private List<SysDept> childrens;

    public List<SysDept> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<SysDept> childrens) {
        this.childrens = childrens;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }
}
