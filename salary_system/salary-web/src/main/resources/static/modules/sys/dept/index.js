layui.config({
    base: Base.ctxPath + '/static/lib/treeTable/',
})
layui.use(['treeTable', 'layer', 'code', 'form'], function () {
    var o = layui.$,
        form = layui.form,
        layer = layui.layer,
        treeTable = layui.treeTable;

    var re = treeTable.render({
        elem: '#tree-table',
        url: Base.ctxPath + "/sys/sysDept/queryDepts",
        icon_key: 'name',
        primary_key: 'id',
        parent_key: 'pId',
        is_checkbox: true,
        cols: [
            // {
            //     key: 'id',
            //     title: 'ID',
            //     width: '10px',
            //     align: 'center',
            // },
            //
            // {
            //     key: 'pId',
            //     title: '父ID',
            //     width: '10px',
            //     align: 'center',
            // },
            {
                key: 'name',
                title: '部门名称',
                width: '100px',
                template: function (item) {
                    return '<span style="color:green;">' + item.name + '</span>';
                }
            },
            // {
            //     key: 'orderNum',
            //     title: '部门排序',
            //     width: '50px'
            // },
        ]
    });


    /**
     * 新增部门
     */
    $("#addDept").click(function () {
        Base.open('添加部门', Base.ctxPath + '/sys/sysDept/dept_add', '480', '460');
    });

    /**
     * 编辑部门
     */
    $("#editDept").click(function () {
        var ids = treeTable.checked(re).join(',');
        if (Base.isBlank(ids)) {
            Base.fail("请至少选择一个部门");
            return false;
        }
        if (ids.split(",").length > 1) {
            Base.fail("只能选择一个部门");
            return false;
        }
        Base.open('编辑部门', Base.ctxPath + '/sys/sysDept/dept_edit?id=' + ids.split(",")[0], '500', '460');
    });
    /**
     * 刷新部门
     */
    $("#refresh").click(function () {
        treeTable.render(re);
    });
    /**
     * 删除部门
     */
    $("#delDept").click(function () {
        var ids = treeTable.checked(re).join(',');
        if (Base.isBlank(ids)) {
            Base.fail("请至少选择一个部门");
            return false;
        }
        $.ajax({
            url: Base.ctxPath + "/sys/sysDept/delDept",
            type: "post",
            data: {
                "ids": ids,
            },
            success: function (result) {
                if (result.success) {
                    Base.alert(result.message);
                    treeTable.render(re);
                } else {
                    Base.fail(result.message);
                    treeTable.render(re);
                }
            }
        });
    });

})