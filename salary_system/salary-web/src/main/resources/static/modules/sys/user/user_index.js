layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/sys/sysUser/queryList'
        , cols: [[
            {type: 'radio'},
            {field: 'id', title: 'ID', hide: true},
            {field: 'userName', title: '登录账号'},
            {field: 'userNick', title: '员工姓名'},
            {field: 'userNo', title: '员工工号'},
            {field: 'userMobile', title: '员工电话'},
            {
                field: 'userSex', title: '员工性别',
                templet: function (d) {
                    if (d.userSex == 1) {
                        return "男";
                    } else if (d.userSex == 2) {
                        return "女"
                    }
                }
            },
            {
                field: 'education', title: '员工学历', templet: function (d) {
                    if (d.type == 1) {
                        return "专科";
                    } else if (d.type == 2) {
                        return "本科";
                    } else if (d.type == 3) {
                        return "研究生"
                    } else if (d.type == 4) {
                        return "博士"
                    } else {
                        return "其他"
                    }
                }
            },
            {
                field: 'post', title: '员工岗位', templet: function (d) {
                    if (d.type == 1) {
                        return "人事专员";
                    } else if (d.type == 2) {
                        return "前台接待";
                    } else if (d.type == 3) {
                        return "部门经理"
                    } else if (d.type == 4) {
                        return "技术员"
                    } else {
                        return "其他"
                    }
                }
            },
            {field: 'entryDate', title: '入职日期'},
            {
                field: 'type', title: '员工类型',
                templet: function (d) {
                    if (d.type == 0) {
                        return "超级管理员";
                    } else if (d.type == 1) {
                        return "普通员工";
                    } else if (d.type == 2) {
                        return "后勤员工"
                    } else if (d.type == 3) {
                        return "管理员工"
                    } else {
                        return "其他"
                    }
                }
            },
            {
                field: 'level', title: '员工等级',
                templet: function (d) {
                    if (d.level == 1) {
                        return "一级"
                    } else if (d.level == 2) {
                        return "二级"
                    } else if (d.level == 3) {
                        return "三级"
                    } else if (d.level == 4) {
                        return "四级"
                    } else if (d.level == 5) {
                        return "五级"
                    } else {
                        return "其他"
                    }
                }
            },
            {field: 'address', title: '员工住址'},
            {
                field: 'status', title: '员工状态',
                templet: function (d) {
                    if (d.status == 1) {
                        return "正常"
                    } else if (d.status == 2) {
                        return "冻结"
                    } else {
                        return "其他"
                    }
                }
            },
        ]]
        , page: true
    });

    //查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: {
                userNick: $("#userNick").val(),
                userNo: $("#userNo").val(),
            }
        });
    }

    form.on('submit(search)',
        function (data) {
            search();
            return false;
        });

    /**
     * 新增
     */
    $("#add").click(function () {
        Base.open('添加', Base.ctxPath + '/sys/sysUser/add', '480', '400');
    });

    /**
     * 编辑
     */
    $("#edit").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('修改', Base.ctxPath + '/sys/sysUser/edit?id=' + data[0].id, '480', '400');
    });

    /**
     * 删除
     */
    $("#del").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.confirm("是否删除该记录？", function () {
            $.ajax({
                url: Base.ctxPath + "/sys/sysUser/del",
                type: "post",
                data: {
                    "id": data[0].id
                },
                success: function (result) {
                    if (result.success) {
                        Base.alert(result.message);
                        search();
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
        });
    });

});