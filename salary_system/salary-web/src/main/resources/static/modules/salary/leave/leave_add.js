layui.use(['form'], function () {
    var o = layui.$,
        form = layui.form;

    form.verify({});

    //监听提交表单
    form.on('submit(add)',
        function (data) {
            $.ajax({
                url: Base.ctxPath + "/salary/leave/save",
                type: "post",
                data: data.field,
                success: function (result) {
                    if (result.success) {
                        Base.success(result.message);
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
            return false;
        });
})
layui.use('laydate', function () {
    var laydate = layui.laydate;
    //执行一个laydate实例
    laydate.render({
        elem: '#leaveDate' //指定元素
        , type: 'date'
        , trigger: 'click'
        , value: new Date()
        // ,
        // done: function (value) {
        //     vm.q.workMonth = value;
        // }
    });
});