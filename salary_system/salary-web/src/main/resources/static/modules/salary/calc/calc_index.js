layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/salary/calc/queryList'
        , cols: [[
            {type: 'radio'},
            {field: 'id', title: 'ID', hide: true},
            {field: 'userName', title: '员工姓名'},
            {field: 'userNo', title: '员工工号'},
            {field: 'calcMonth', title: '核算月份'},
            {
                field: 'status', title: '核算状态',
                templet: function (d) {
                    if (d.status == 1) {
                        return "未核算";
                    } else if (d.status == 2) {
                        return "已核算"
                    }
                }
            }
        ]]
        , page: true
    });


    //查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: {
                calcMonth: $("#calcMonth").val(),
                status: $("#status").val()
            }
        });
    }
    form.on('submit(search)',
        function (data) {
            search();
            return false;
        });
    /**
     * 新增
     */
    $("#add").click(function () {
        Base.open('启动核算', Base.ctxPath + '/salary/calc/start', '480', '400');
    });

    /**
     * 核算
     */
    $("#edit").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('核算', Base.ctxPath + '/salary/calc/edit?id=' + data[0].id, '480', '400');
    });

    /**
     * 删除
     */
    $("#del").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.confirm("是否删除该记录？", function () {
            $.ajax({
                url: Base.ctxPath + "/salary/calc/del",
                type: "post",
                data: {
                    "id": data[0].id
                },
                success: function (result) {
                    if (result.success) {
                        Base.alert(result.message);
                        search();
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
        });
    });

});
layui.use('laydate', function () {
    var laydate = layui.laydate;

    //执行一个laydat
    laydate.render({
        elem: '#calcMonth' //指定元素
        , type: 'month'
        , value: '2020-04'
    });
});