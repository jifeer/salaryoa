layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/salary/work/record/queryList'
        , cols: [[
            {type: 'radio'},
            {field: 'id', title: 'ID', hide: true},
            {field: 'userName', title: '员工姓名'},
            {field: 'userNo', title: '员工工号'},
            {field: 'workDate', title: '出勤日期'},
            {field: 'upTime', title: '上班时间'},
            {field: 'downTime', title: '下班时间'},
            {
                field: 'workStatus', title: '上班状态',
                templet: function (d) {
                    if (d.workStatus == 1) {
                        return "正常";
                    } else if (d.workStatus == 2) {
                        return "迟到"
                    } else if (d.workStatus == 3) {
                        return "早退"
                    } else if (d.workStatus == 4) {
                        return "加班"
                    }else if (d.workStatus == 5) {
                        return "请假"
                    }
                }
            }
        ]]
        , page: true
    });


    //查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: {
                employeeId: $("#employeeId").val(),
                workDate: $("#workDate").val()
            }
        });
    }

    form.on('submit(search)',
        function (data) {
            search();
            return false;
        });
    /**
     * 上班
     */
    $("#upTime").click(function () {
        Base.open('上班打卡', Base.ctxPath + '/salary/work/record/upTime', '480', '400');
    });
    /**
     * 下班
     */
    $("#downTime").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('下班打卡', Base.ctxPath + '/salary/work/record/downTime?id=' + data[0].id, '480', '400');
    });
});
layui.use('laydate', function () {
    var laydate = layui.laydate;

    //执行一个laydat
    laydate.render({
        elem: '#workDate' //指定元素
        , value: new Date()
    });
});