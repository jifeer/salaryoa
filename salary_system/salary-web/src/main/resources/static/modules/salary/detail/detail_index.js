layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/salary/detail/queryList?recordId=' + $("#recordId").val()
        , cols: [[
            {type: 'numbers'},
            {field: 'id', title: 'ID', hide: true},
            {
                field: 'costName',
                title: '项目名称',
                with:100
            },
            {
                field: 'type',
                title: '类型',
                templet: function (d) {
                    if (d.type == 1) {
                        return "增加";
                    } else if (d.type == 2) {
                        return "扣减"
                    }
                }                ,
                with:50
            },
            {field: 'costAmount', title: '金额',with:50}
        ]]
        ,
        page: true
    });


//查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
        });
    }

    /**
     * 新增字典值
     */
    $("#add").click(function () {
        Base.open('添加', Base.ctxPath + '/salary/detail/add', '480', '400');
    });

    /**
     * 编辑
     */
    $("#edit").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('修改', Base.ctxPath + '/salary/detail/edit?id=' + data[0].id, '480', '400');
    });

    /**
     * 删除
     */
    $("#del").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.detailirm("是否删除该记录？", function () {
            $.ajax({
                url: Base.ctxPath + "/salary/detail/del",
                type: "post",
                data: {
                    "id": data[0].id
                },
                success: function (result) {
                    if (result.success) {
                        Base.alert(result.message);
                        search();
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
        });
    });

})
;