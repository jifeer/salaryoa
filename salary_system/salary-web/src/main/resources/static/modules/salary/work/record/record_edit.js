layui.use(['form'], function () {
    var o = layui.$,
        form = layui.form;

    form.verify({});

    form.on('submit(edit)',
        function (data) {
            $.ajax({
                url: Base.ctxPath + "/salary/work/record/update",
                type: "post",
                data: data.field,
                success: function (result) {
                    if (result.success) {
                        Base.success(result.message);
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
            return false;
        });
})
layui.use('laydate', function(){
    var laydate = layui.laydate;

    //执行一个laydate实例
    laydate.render({
        elem: '#workDate' //指定元素
        ,value: new Date()
    });
    laydate.render({
        elem: '#downTime'
        ,type: 'time'
        ,value: new Date()
    });
});
