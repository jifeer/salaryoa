layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/salary/conf/queryList'
        , cols: [[
            {type: 'radio'},
            {field: 'id', title: 'ID',hide:true},
            {field: 'typeName', title: '项目名称'},
            {
                field: 'type', title: '项目类型',
                templet: function (d) {
                    if (d.type == 1) {
                        return "增加";
                    } else if (d.type == 2) {
                        return "扣减"
                    }
                }
            },
            {
                field: 'calcRule', title: '计算规则',
                templet: function (d) {
                    if (d.calcRule == 1) {
                        return "直接";
                    } else if (d.calcRule == 2) {
                        return "计算"
                    }
                }
            },
            {field: 'amount', title: '金额'}
        ]]
        , page: true
    });


    //查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: {
                type: $("#type").val(),
                calcRule: $("#calcRule").val()
            }
        });
    }
    form.on('submit(search)',
        function (data) {
            search();
            return false;
        });
    /**
     * 新增
     */
    $("#add").click(function () {
        Base.open('新增', Base.ctxPath + '/salary/conf/add', '480', '400');
    });

    /**
     * 编辑
     */
    $("#edit").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('修改', Base.ctxPath + '/salary/conf/edit?id=' + data[0].id, '480', '400');
    });

    /**
     * 删除
     */
    $("#del").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.confirm("是否删除该记录？", function () {
            $.ajax({
                url: Base.ctxPath + "/salary/conf/del",
                type: "post",
                data: {
                    "id": data[0].id
                },
                success: function (result) {
                    if (result.success) {
                        Base.alert(result.message);
                        search();
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
        });
    });

});