layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/salary/record/queryList'
        , cols: [[
            {type: 'radio'},
            {field: 'id', title: 'ID',hide:true},
            {field: 'userName',title: '员工姓名'},
            {field: 'userNo',title: '员工工号'},
            {field: 'salaryDate',title: '工资月份'},
            {field: 'mustSalary', title: '应发工资'},
            {field: 'realitySalary', title: '实发工资'}
        ]]
        , page: true
    });


    //查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: {
                employeeId: $("#employeeId").val(),
                salaryDate: $("#salaryDate").val()
            }
        });
    }

    form.on('submit(search)',
        function (data) {
            search();
            return false;
        });
    /**
     * 工资明细
     */
    $("#detail").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('工资明细', Base.ctxPath + '/salary/detail/index?id=' + data[0].id, '600', '500');
    });
    /**
     * 导出Excel
     */
    $("#export").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        window.location.href=Base.ctxPath + "/salary/record/export?id="+data[0].id;
    });
});
layui.use('laydate', function(){
    var laydate = layui.laydate;

    //执行一个laydat
    laydate.render({
        elem: '#salaryDate' //指定元素
        ,type: 'month'
        ,value: new Date()
    });
});