layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/salary/leave/queryList'
        , cols: [[
            {type: 'radio'},
            {field: 'id', title: 'ID',hide:true},
            {field: 'leaveDate',title: '请假日期'},
            {field: 'reason',title: '请假原因'},
            {field: 'leaveName',title: '请假人'},
            {field: 'approvalName',title: '审批人'},
            {field: 'approvalResult',title: '审批结果',
                templet: function (d) {
                    if (d.approvalResult == 0) {
                        return "待审批";
                    } else if (d.approvalResult == 1) {
                        return "通过"
                    } else if (d.approvalResult == 2) {
                        return "未通过"
                    }
                }},
            {field: 'approvalReason',title: '审批意见'},
            {field: 'approvalTime',title: '审批时间'}
            // {field: 'status',title: '状态',
            //     templet: function (d) {
            //         if (d.status == 0) {
            //             return "待审批";
            //         } else if (d.status == 1) {
            //             return "通过";
            //         } else if (d.status == 2) {
            //             return "驳回"
            //         }
            //     }},
            // {field: 'createTime',title: '创建时间'}
        ]]
        , page: true
    });


    //查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: {
                title: $("#title").val(),
                leaveId: $("#leaveId").val(),
                leaveDate: $("#leaveDate").val()
            }
        });
    }
    form.on('submit(search)',
        function (data) {
            search();
            return false;
        });
    /**
     * 新增
     */
    $("#add").click(function () {
        Base.open('新增', Base.ctxPath + '/salary/leave/add', '480', '400');
    });

    /**
     * 批量新增
     */
    $("#batchAdd").click(function () {
        $.ajax({
            url: Base.ctxPath + "/salary/leave/batchAdd",
            type: "post",
            success: function (result) {
                if (result.success) {
                    Base.alert(result.message);
                    search();
                } else {
                    Base.fail(result.message);
                }
            }
        });
    });
    /**
     * 编辑
     */
    $("#edit").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('修改', Base.ctxPath + '/salary/leave/edit?id=' + data[0].id, '480', '400');
    });

    $("#approval").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('审批', Base.ctxPath + '/salary/leave/approval?id=' + data[0].id, '480', '400');
    });
    /**
     * 删除
     */
    $("#del").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.confirm("是否删除该记录？", function () {
            $.ajax({
                url: Base.ctxPath + "/salary/leave/del",
                type: "post",
                data: {
                    "id": data[0].id
                },
                success: function (result) {
                    if (result.success) {
                        Base.alert(result.message);
                        search();
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
        });
    });

});
layui.use('laydate', function () {
    var laydate = layui.laydate;
    //执行一个laydat
    laydate.render({
        elem: '#leaveDate' //指定元素
        , value: new Date()
    });
});