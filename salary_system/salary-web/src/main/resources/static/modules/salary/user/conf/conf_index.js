layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/salary/user/conf/queryList'
        , cols: [[
            {type: 'radio'},
            {field: 'id', title: 'ID',hide:true},
            {field: 'userName',title: '员工姓名'},
            {field: 'userNo',title: '员工工号'},
            {field: 'salaryItem',title: '工资项'},
            {field: 'salaryConfRatio',title: '工资项系数'}
        ]]
        , page: true
    });


    //查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: {
                employeeId: $("#employeeId").val(),
            }
        });
    }
    form.on('submit(search)',
        function (data) {
            search();
            return false;
        });
    /**
     * 新增
     */
    $("#add").click(function () {
        Base.open('新增', Base.ctxPath + '/salary/user/conf/add', '480', '400');
    });
    /**
     * 批量新增
     */
    $("#batchAdd").click(function () {
        $.ajax({
            url: Base.ctxPath + "/salary/user/conf/batchAdd",
            type: "post",
            success: function (result) {
                if (result.success) {
                    Base.alert(result.message);
                    search();
                } else {
                    Base.fail(result.message);
                }
            }
        });
    });
    /**
     * 编辑
     */
    $("#edit").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('修改', Base.ctxPath + '/salary/user/conf/edit?id=' + data[0].id, '480', '400');
    });

    /**
     * 删除
     */
    $("#del").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.confirm("是否删除该记录？", function () {
            $.ajax({
                url: Base.ctxPath + "/salary/user/conf/del",
                type: "post",
                data: {
                    "id": data[0].id
                },
                success: function (result) {
                    if (result.success) {
                        Base.alert(result.message);
                        search();
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
        });
    });

});