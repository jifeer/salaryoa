layui.use(['table', 'form'], function () {
    var table = layui.table,
        form = layui.form;

    table.render({
        elem: '#tb'
        , url: Base.ctxPath + '/salary/notice/queryList'
        , cols: [[
            {type: 'radio'},
            {field: 'id', title: 'ID',hide:true},
            {field: 'title',title: '公告主题'},
            {field: 'content',title: '公告内容'},
            {field: 'releaseName',title: '发布人'},
            {field: 'releaseTime',title: '发布时间'}
        ]]
        , page: true
    });


    //查询
    function search() {
        table.reload('tb', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: {
                title: $("#title").val(),
            }
        });
    }
    form.on('submit(search)',
        function (data) {
            search();
            return false;
        });
    /**
     * 新增
     */
    $("#add").click(function () {
        Base.open('新增', Base.ctxPath + '/salary/notice/add', '480', '400');
    });
    /**
     * 批量新增
     */
    $("#batchAdd").click(function () {
        $.ajax({
            url: Base.ctxPath + "/salary/notice/batchAdd",
            type: "post",
            success: function (result) {
                if (result.success) {
                    Base.alert(result.message);
                    search();
                } else {
                    Base.fail(result.message);
                }
            }
        });
    });
    /**
     * 编辑
     */
    $("#edit").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.open('修改', Base.ctxPath + '/salary/notice/edit?id=' + data[0].id, '480', '400');
    });

    /**
     * 删除
     */
    $("#del").click(function () {
        var data = Base.getSelected(table, 'tb');
        if (data.length == 0) {
            Base.fail("至少选择一行数据");
            return false;
        }
        Base.confirm("是否删除该记录？", function () {
            $.ajax({
                url: Base.ctxPath + "/salary/notice/del",
                type: "post",
                data: {
                    "id": data[0].id
                },
                success: function (result) {
                    if (result.success) {
                        Base.alert(result.message);
                        search();
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
        });
    });

});