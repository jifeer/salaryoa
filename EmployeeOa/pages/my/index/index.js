var Function = require("../../../utils/function.js");
import request from '../../../utils/request.js'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    qrCode: false, //是否展示二维码
    detail: {}, //个人信息
    daka_code: '',            //打卡的状态
    userNick: '',
    userEmail: ''
  },
  goToWork() {  //上班打卡
    var userDetaile = wx.getStorageSync("user")
    var employeeNum = userDetaile.id
    var timestamp = Date.parse(new Date());
    var date = new Date(timestamp);
    //获取年  
    var Y = date.getFullYear();
    //获取月  
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
    //获取日
    var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var hour = date.getHours()< 10 ? '0' + date.getHours() : date.getHours();
    var minute = date.getMinutes()< 10 ? '0' + date.getMinutes() : date.getMinutes();
    var second = date.getSeconds()< 10 ? '0' + date.getSeconds() : date.getSeconds();
    var curDate = Y + "-" + M + "-" + D;
    var curHour = hour + ":" + minute + ":" + second;
    request({
      url: '/api/goToWork',
      method: 'post',
      data: {
        employeeId: employeeNum,
        workDate: curDate,
        upTime: curHour
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },
    }).then(res => {
      console.log(res, '上班打卡')
      if (res.data.code == 200) {
        wx.showToast({
          title: "上班打卡成功！",
          icon: 'success',
          duration: 1500
        })
        setTimeout(() => {
          this.setData({
            clockShow: false
          })
        }, 1200)
      } else {
        Function.layer(res.data.message)
      }
    })
  },
  goOffWork() {
    var userDetaile = wx.getStorageSync("user")
    var employeeNum = userDetaile.id
    var timestamp = Date.parse(new Date());
    var date = new Date(timestamp);
    //获取年  
    var Y = date.getFullYear();
    //获取月  
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
    //获取日
    var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var hour = date.getHours()< 10 ? '0' + date.getHours() : date.getHours();
    var minute = date.getMinutes()< 10 ? '0' + date.getMinutes() : date.getMinutes();
    var second = date.getSeconds()< 10 ? '0' + date.getSeconds() : date.getSeconds();
    var curDate = Y + "-" + M + "-" + D;
    var curHour = hour + ":" + minute + ":" + second;
    request({
      url: '/api/goOffWork',
      method: 'post',
      data: {
        employeeId: employeeNum,
        workDate: curDate,
        downTime: curHour
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },
    }).then(res => {
      console.log(res, '下班打卡')
      if (res.data.code == 200) {
        wx.showToast({
          title: '下班打卡成功！',
          icon: 'success',
          duration: 1500
        })
        setTimeout(() => {
          this.setData({
            clockShow: false
          })
        }, 1200)
      }else{
        Function.layer(res.data.message)
      }
    })
  },
  showQrcode() {
    // console.log('二维码')
    this.setData({
      qrCode: true
    })
    // console.log(this.data.qrCode)
  },
  logout() {
    console.log("登出了。。。。")
    wx.removeStorageSync("user"),
      wx.redirectTo({
        url: "/pages/authorization/login/index"
      })
  },
  hideQrcode() {
    this.setData({
      qrCode: false
    })
  },
  clockShow() {
    this.setData({
      clockShow: true
    })
  },
  hideClock() {
    this.setData({
      clockShow: false
    })
  },
  // 获取个人信息


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var userDetaile = wx.getStorageSync("user")
    var nick = userDetaile.userNick
    var mail = userDetaile.userEmail
    this.setData({
      userNick: nick,
      userEmail: mail
    })
  },

  onShow: function () {
  },
  onHide: function () {
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    setTimeout(() => {
      this.setData({
        avatar: ''
      })
      this.userInfo();
      wx.stopPullDownRefresh();
    }, 1000)
  },
})