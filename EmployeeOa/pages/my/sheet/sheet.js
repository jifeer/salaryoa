// pages/my/inform/inform.js
var Function = require("../../../utils/function.js");
import request from '../../../utils/request.js'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    list: [],//列表
    pages: 1
  },

  onLoad: function (options) {
    this.list();
  },
  list() {  //通知列表
    var userDetaile = wx.getStorageSync("user")
    var employeeNum = userDetaile.id
    request({
      url: '/api/getWorkRecords',
      data: {
        employeeId: employeeNum
      },
    }).then(res => {
      console.log(res, '考勤列表')
      if (res.data.code == 200) {
        this.setData({
          list: res.data.data
        })
      } else {
        Function.layer('获取考勤失败，请稍后重试！')
      }
    })
  },
  onPullDownRefresh: function () {

    setTimeout(() => {
      this.setData({
        list: [],
        pages: 1
      })

      this.list();
      wx.stopPullDownRefresh()
    }, 2000)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    wx.showLoading({
      title: '正在加载',
      mask: true
    })

    setTimeout(() => {
      this.setData({
        pages: this.data.pages++
      })
      this.list();
      wx.hideLoading()
    }, 1000)
  }
})