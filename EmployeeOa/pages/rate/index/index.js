// pages/rate/index/index.js
var Function = require("../../../utils/function.js");
import request from '../../../utils/request.js'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    salaryRecord: '',
  },
  getDetail() {  //获取收益详情
    var userDetaile = wx.getStorageSync("user")
    console.log(userDetaile.userNo, '员工编号')
    var timestamp = Date.parse(new Date());
    var date = new Date(timestamp);
    //获取年  
    var Y = date.getFullYear();
    //获取月  
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
    //获取日
    var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var employeeId = userDetaile.id
    var searchDate = Y + "-" + M;
    console.log(searchDate, '时间')
    request({
      url: '/api/getXZDetail',
      method: 'post',
      data: {
        employeeId: employeeId,
        searchDate: searchDate
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },
    }).then(res => {
      console.log(res.data.obj.salaryRecord, '薪资信息')
      console.log(res.data.obj.salaryDetails, '薪资信息')
      if (res.data.code == 200) {
        this.setData({
          salaryRecord: res.data.obj.salaryRecord,
          list:res.data.obj.salaryDetails
        })
      } else if (res.data.code == 500) {
        Function.layer(res.data.msg);
        setTimeout(() => {
          wx.navigateTo({
            url: '/pages/authorization/auth/auth',
          })
        }, 1000)
      } else {
        Function.layer('获取薪资情失败,请稍后重试！')
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  onShow() {
    this.getDetail(); //调用详情
  }
})