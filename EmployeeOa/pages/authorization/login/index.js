var Function = require("../../../utils/function.js");
import request from '../../../utils/request.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone: '',
    password: '',
    is_wx: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  closeLogin() { //关闭微信登录
    this.setData({
      is_wx: false
    })
  },
  openLogin() {
    this.setData({
      is_wx: true
    })
  },
  handlePhone(e) {
    this.setData({
      phone: e.detail.value
    })
  },
  handleMima(e) {
    // console.log(e)
    this.setData({
      password: e.detail.value
    })
  },
  handle_login() { //点击登录
    if (this.data.phone == '') {
      Function.layer('请输入账号！')
      return false;
    }
    if (this.data.password == '') {
      Function.layer('密码不能为空！')
      return false;
    }
    request({
      url: '/api/wxDoLogin',
      method: 'post',
      data: {
        account: this.data.phone,
        password: this.data.password
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },
    }).then(res => {
      console.log(res.data.obj)
      if (res.data.code == 500) {
        wx.showModal({
          title: '提示',
          content: '该账号不存在,请重新输入！',
          success: res => {
            if (res.confirm) {
              this.setData({
                password: '',
                phone: ''
              })
            }
          }
        })
      } else if (res.data.code == 200) {  //登录成功
        Function.layer('登录成功!')
        wx.setStorageSync('user', res.data.obj)  //保存信息
        setTimeout(() => {
          wx.switchTab({
            url: '/pages/rate/index/index'
          })
        }, 1000)

      } else {
        Function.layer('登录失败,请重新登录!')
        this.setData({
          password: '',
          phone: ''
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

})